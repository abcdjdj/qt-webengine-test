#include <QApplication>
#include <QMainWindow>

#include <QByteArray>
#include <QDebug>
#include <QFile>
#include <QGraphicsView>
#include <QWebEngineView>
#include <QShortcut>

#include <QUrl>

#include "svgimageview.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    const QUrl url("file:///home/madhav/Downloads/SvgTest/test.svg");

    SvgImageView *mImageView = new SvgImageView(url);
    QGraphicsScene scene;
    scene.addItem(mImageView->webEngineView());
    QGraphicsView gView(&scene);

    gView.show();

    return a.exec();
}
