#include <QWebEngineSettings>
#include <QGraphicsWidget>
#include <QTimer>

#include "svgimageview.h"

SvgImageView::SvgImageView(const QUrl &url)
: mView(new QWebEngineView())
, mSetWindowSize(true)
, mGraphicsPixmapItem(new QGraphicsPixmapItem)
{
    mView->setAttribute(Qt::WA_DontShowOnScreen, true);
    mView->setAttribute(Qt::WA_DeleteOnClose, true);

    mView->page()->settings()->setAttribute(QWebEngineSettings::ShowScrollBars, false);
    mView->page()->settings()->setAttribute(QWebEngineSettings::WebAttribute::LocalContentCanAccessFileUrls, true);

    QObject::connect(mView, &QWebEngineView::loadFinished, this, &SvgImageView::onLoadFinished);
    QObject::connect(mView->page(), &QWebEnginePage::contentsSizeChanged, this, &SvgImageView::onContentsSizeChanged);

    mView->load(url);
    mView->show();
}

SvgImageView::~SvgImageView()
{
    delete mGraphicsPixmapItem;
    delete mView;
}

QGraphicsItem *SvgImageView::webEngineView()
{
    return mGraphicsPixmapItem;
}

void SvgImageView::onContentsSizeChanged()
{
    QSize size = mView->page()->contentsSize().toSize();
    qDebug() << size.width() << "x" << size.height();

    // Adjust size, upscale if needed
    qreal zoomFactor = 1.0;
    while (size.width() < 2000 && size.height() < 2000) {
        size = size * 2;
    }

    zoomFactor = size.height()/mView->page()->contentsSize().toSize().height();
    if (mSetWindowSize) {
        mView->setZoomFactor(zoomFactor);
        mView->resize(size);
        qDebug() << "Zoom = " << mView->zoomFactor();
        //mView->reload();
    }
    mSetWindowSize = false;
}

void SvgImageView::takeScreenshot()
{
    qDebug() << "Taking screenie!";
    QSize size = mView->page()->contentsSize().toSize();

    QPixmap image(size);
    QPainter painter(&image);
    mView->page()->view()->render(&painter);
    painter.end();

    image.save("/home/madhav/Desktop/image.png", "png", 100);
    mGraphicsPixmapItem->setPixmap(image);
}

void SvgImageView::onLoadFinished(bool ok)
{
    qDebug() << "onLoadFinished Success = " << ok;
    QTimer::singleShot(1500, this, &SvgImageView::takeScreenshot);
}
