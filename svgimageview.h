#ifndef SVGIMAGEVIEW_H
#define SVGIMAGEVIEW_H

#include <QWebEngineView>
#include <QObject>
#include <QUrl>
#include <QGraphicsProxyWidget>

class SvgImageView : public QObject
{
    Q_OBJECT

private:
    QWebEngineView *mView;
    bool mSetWindowSize;
    QGraphicsPixmapItem *mGraphicsPixmapItem;
    ~SvgImageView();

public:
    SvgImageView(const QUrl &url);
    QGraphicsItem *webEngineView();


protected Q_SLOTS:
    void onContentsSizeChanged();
    void onLoadFinished(bool ok);
    void takeScreenshot();
};

#endif // SVGIMAGEVIEW_H
